<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>05</title>
		<link rel="stylesheet" type="text/css" href="style.css">
	</head>
	<body>
    <?php
      $player01 = array(
        "id" => "3",
        "name" => "梶谷隆幸",
        "position"  => "外野手",
        "from" => "島根",
        "year" => "2007",
      );
      $player02 = array(
        "id" => "44",
        "name" => "佐野恵太",
        "position"  => "外野手",
        "from" => "岡山",
        "year" => "2017",
      );
      $player03 = array(
        "id" => "15",
        "name" => "井納翔一",
        "position"  => "投手",
        "from" => "東京",
        "year" => "2013",
      );
      $players = array($player01,$player02,$player03);

      foreach($players as $each){
        echo "背番号: " . $each['id'] . ", "
             . "名前: " . $each['name'] . ", "
             . "ポジション: " . $each['position'] . ", "
             . "出身地: " . $each['from'] . ", "
             . "入団年: " . $each['year'] . "<br/>";
      }

      echo "<hr/>";

      echo "<table border='1' cellspacing='0'>";
        echo "<tr>";
          echo "<th>背番号</th><th>名前</th><th>ポジション</th><th>出身地</th><th>入団年</th>";
        echo "</tr>";
      foreach($players as $each){
        echo "<tr>";
          echo "<td>" . $each['id'] . "</td>"
              . "<td>" . $each['name'] . "</td>"
              . "<td>" . $each['position'] . "</td>"
              . "<td>" . $each['from'] . "</td>"
              . "<td>" . $each['year'] . "年</td>";
        echo "</tr>";
      }
      echo "</table>";

      //var_dump($players);
     ?>
	</body>


</html>
