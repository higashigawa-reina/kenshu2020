<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>第144回 ワグネル定演</title>
		<link rel="stylesheet" type="text/css" href="style.css">
	</head>
	<body>
    <?php
      $stage01 = array(
        "stage" => "1",
        "poem" => "いろいろ",
        "music"  => "Ēriks Ešenvalds 他",
        "title" => "Colors ~ラトビアの人と風景~",
        "conductor" => "佐藤正浩",
      );
      $stage02 = array(
        "stage" => "2",
        "poem" => "竹久夢二",
        "music"  => "森田花央里",
        "title" => "男声合唱組曲『青い小径』・「みどりの窓」",
        "conductor" => "山内祥平",
      );
      $stage03 = array(
        "stage" => "3",
        "poem" => "銀色夏生",
        "music"  => "上田真樹",
        "title" => "男声合唱組曲『終わりのない歌』",
        "conductor" => "清水雅彦",
      );
      $stage04 = array(
        "stage" => "4",
        "poem" => "DARION, Joe",
        "music"  => "LEIGH, Mitch",
        "title" => "男声合唱のための組曲『Man of La Mancha』",
        "conductor" => "佐藤正浩",
      );
      $stages = array($stage01, $stage02, $stage03, $stage04);
      ?>

    <hr/>

    <table border='1' cellspacing='0'>
      <tr>
        <th>ステージ</th><th>作詞</th><th>作曲</th><th>曲名</th><th>指揮者</th>
      </tr>
      <?php
        foreach($stages as $each){
          echo "<tr>";
            echo "<td>第" . $each['stage'] . "ステージ</td>"
                . "<td>" . $each['poem'] . "</td>"
                . "<td>" . $each['music'] . "</td>"
                . "<td>" . $each['title'] . "</td>"
                . "<td>" . $each['conductor'] . "</td>";
          echo "</tr>";
        }
      ?>
      </table>


      <pre>
        <?php var_dump($stages) ?>
      </pre>

	</body>


</html>
