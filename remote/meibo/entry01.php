<!DOCTYPE html>
<link rel = "stylesheet" type="text/css" href = "./include/style.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<?php
    include("./include/statics.php");
    include("./include/functions.php");
    $pdo = initDB();

    $query_str = "SELECT m.member_ID, m.name, m.seibetu, m.age, sm.section_name, gm.grade_name
	              FROM member AS m
                  LEFT JOIN section1_master AS sm ON m.section_ID = sm.ID
                  LEFT JOIN grade_master AS gm ON m.grade_ID = gm.ID
                  WHERE 1=1";

    $temp_busho = '';
    if(isset($_GET['busho'])){
    $temp_busho = $_GET['busho'];
    }

    $temp_yakushoku = '';
    if(isset($_GET['yakushoku'])){
    $temp_yakushoku = $_GET['yakushoku'];
    }

?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device=width, initial-scale-1">
    <title>新規社員情報登録</title>
    <script src="include/functions.js"></script>
  </head>
  <body>
      <?php include("./include/header.php"); ?>
      <form method="post" action="entry02.php" name="mainform">
          <table class="table" style="text-align:center; width: 50%;">
              <tr>
                  <th scope='row'>名前</th>
                  <td style="text-align:left;"><input type='text' name="namae" maxlength='30' placeholder='名前を入力'
                      class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" style="width:70%"></td>
              </tr>
              <tr>
                  <th scope='row'>出身地</th>
                  <td style="text-align:left;"><select name="pref"  class="form-control" id="exampleFormControlSelect1" style="width:30%">
                      <option value="">都道府県</option>
                      <?php foreach ($pref_array as $key => $value) {
                                echo "<option value ='" . $key . "'>" . $value . "</option>";// code...
                            }
                       ?>
                      </select>
                  </td>
              </tr>
              <tr>
                  <th scope='row'>性別</th>
                  <td style="text-align:left;">
                    <?php
                        foreach ($gender_array as $key => $value) {
                            if($key == 1){
                                echo "<label><input type='radio' name='seibetu' value='" . $key . "' checked>" . $value . "　　</label>";
                            }else {
                                echo "<label><input type='radio' name='seibetu' value='" . $key . "'>" . $value . "　　</label>";
                            }
                        }
                   ?>
                  </td>
              </tr>
              <tr>
                  <th scope='row'>年齢</th>
                  <td style="text-align:left;" class="form-inline">
                      <input type="number" name="age" maxlength='2' placeholder='年齢を入力'
                       class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" style="width:150px;">　才</td>
              </tr>
              <tr>
                  <th scope='row'>所属部署</th>
                  <td style="text-align:left;">
                    <?php
                        $result = getSection();
                        foreach ($result as $each){
                            if($each['ID'] == 1){
                                echo "<label><input type='radio' name='section' value='" . $each['ID'] . "' checked>" . $each['section_name'] . "　　</option></label>";
                            }else {
                                echo "<label><input type='radio' name='section' value='" . $each['ID'] . "'>" . $each['section_name'] . "　　</option></label>";
                            }
                        }
                     ?>
                  </td>
              </tr>
              <tr>
                  <th scope='row'>役職</th>
                  <td style="text-align:left;">
                    <?php
                        $result = getGrade();
                        foreach ($result as $each){
                            if($each['ID'] == 1){
                                echo "<label><input type='radio' name='grade' value='" . $each['ID'] . "' checked>" . $each['grade_name'] . "　　</option></label>";
                            }else {
                                echo "<label><input type='radio' name='grade' value='" . $each['ID'] . "'>" . $each['grade_name'] . "　　</option></label>";
                            }
                        }
                     ?>
                  </td>
              </tr>
          </table>
          <div style="text-align:center">
          <button type="button" class="btn btn-outline-info" onclick="conf();">登録</button>
          <button type="reset" class="btn btn-outline-secondary" onclick="clearForm();">リセット</button>
          </div>
      </form>
      <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
  </html>
