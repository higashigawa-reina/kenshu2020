<!DOCTYPE html>
<link rel = "stylesheet" type="text/css" href = "./include/style.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<?php
    $DB_DSN = "mysql:host=localhost; dbname=rhigashigawa; chartset=utf8";    // host=localhost・・・自分のですよ。utf8のところが違うと文字化けするよ。
    $DB_USER = "webaccess";
    $DB_PW = "toMeu4rH";      // IDとPWがめちゃめちゃ大事！！！見ることが出来る人・編集できる人を厳格に定義。
    $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);    // 引数としてPDOに投げ込むと、接続を確立する。　ここまで4行はおまじない。


    $temp_member_ID = "";
    if(isset($_GET['member_ID']) && $_GET['member_ID'] !=""){
        $temp_member_ID = $_GET['member_ID'];
    }


    $query_str = "SELECT m.member_ID, m.name, m.pref, m.seibetu, m.age, sm.section_name, gm.grade_name
                  FROM member AS m
                  LEFT JOIN section1_master AS sm ON m.section_ID = sm.ID
                  LEFT JOIN grade_master AS gm ON m.grade_ID = gm.ID
                  WHERE member_ID = " . $temp_member_ID;




    //echo $query_str;  //デバックプリント
    $sql = $pdo -> prepare($query_str);   // $query_strに$pdoを入れる
    $sql -> execute();
    $result = $sql -> fetchAll(); // $SQLの実行結果が$resultに反映される　ここもおまじない。




    include("./include/statics.php");

 ?>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>社員情報詳細</title>
		<link rel="stylesheet" type="text/css" href="./style.css">

        <script type="text/javascript">
            function conf(){
                if(window.confirm('データを削除します。よろしいですか？')){
                    document.mainform.submit();
                }
                //alert("Hello, JavaScript.");
                /*var element = document.getElementById("yoisho");
                element.textContent = "ビール腹";*/
            }
        </script>
	</head>
	<body>
        <?php
            include("./include/header.php");
            if(count($result) !=1){ ?>
                <div style='text-align:center'>不正な処理がなされました。<a href = './index.php'>トップ画面</a>へ戻ってください。</div>
        <?php
            }else {
        ?>
                <table class='table' style='text-align:center; width: 50%;'>
                    <tbody>
                        <tr>
                            <th scope='row'>社員ID</th>
                            <td><?php echo $result[0]['member_ID']; ?></td>
                        </tr>
                        <tr>
                            <th scope='row'>名前</th>
                            <td><?php echo $result[0]['name']; ?></td>
                        </tr>
                        <tr>
                            <th scope='row'>出身地</th>
                            <td><?php echo $pref_array[$result[0]['pref']]; ?></td>
                        </tr>
                        <tr>
                            <th scope='row'>性別</th>
                            <td><?php echo $gender_array[$result[0]['seibetu']]; ?></td>
                        </tr>
                        <tr>
                            <th scope='row'>年齢</th>
                            <td><?php echo  $result[0]['age']; ?></td>
                        </tr>
                        <tr>
                            <th scope='row'>所属部署</th>
                            <td><?php echo $result[0]['section_name']; ?></td>
                        </tr>
                        <tr>
                            <th scope='row'>役職</th>
                            <td><?php echo $result[0]['grade_name']; ?></td>
                        </tr>

        </table>
        <div style='text-align:center'>
        <form method='post' action='entry_update01.php' style='display:inline'>
            <input type='hidden' name='member_ID' value='<?php echo $temp_member_ID; ?>'>
            <button type='submit' class='btn btn-outline-info'>編集</button>
        </form>
        <form method='post' action='delete01.php' name='mainform' style='display:inline'>
            <input type='hidden' name='member_ID' value='<?php echo  $temp_member_ID; ?>'>
            <button type='button' class='btn btn-outline-secondary' onclick='conf();'>削除</button>
        </form>
        </div>
        <?php
            }
        ?>
        <pre><?php //var_dump($result); ?></pre>
        <pre><?php //var_dump($_GET); ?></pre>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	</body>
 </html>
