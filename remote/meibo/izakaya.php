<!DOCTYPE html>
<link rel = "stylesheet" href = "./include/style.css">
<?php
  $DB_DSN = "mysql:host=localhost; dbname=rhigashigawa; chartset=utf8";    // host=localhost・・・自分のですよ。utf8のところが違うと文字化けするよ。
  $DB_USER = "webaccess";
  $DB_PW = "toMeu4rH";      // IDとPWがめちゃめちゃ大事！！！見ることが出来る人・編集できる人を厳格に定義。
  $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);    // 引数としてPDOに投げ込むと、接続を確立する。　ここまで4行はおまじない。

  $query_str = "SELECT *
                FROM izakaya";  // 投げたいSQLを$query_strで保持

  echo $query_str;  //デバックプリント
  $sql = $pdo -> prepare($query_str);   // $query_strに$pdoを入れる
  $sql -> execute();
  $result = $sql -> fetchAll(); // $SQLの実行結果が$resultに反映される　ここもおまじない。

 ?>

<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device=width, initial-scale-1">
    <title>居酒屋ウェブレッジ水道橋店</title>
  </head>
  <body>

    <pre>
      <?php
       // var_dump($result);
       ?>
    </pre>

    <?php
    foreach($result as $each){
        // var_dump($each);
        echo $each['dish_name'] . " : " . $each['price'] . " 円";
        echo "<hr/>";
    }
    ?>

    <hr/>
    <table border='1' cellspacing='0'>
        <tr>
            <th>id</th><th>品名</th><th>ジャンル</th><th>価格</th><th>メモ</th>
        </tr>
        <?php
            foreach ($result as $each) {
                echo "<tr>";
                    echo "<td>" . $each['id'] . "</td>"
                        . "<td>" . $each['dish_name'] . "</td>"
                        . "<td>" . $each['genre'] . "</td>"
                        . "<td>" . $each['price'] . "</td>"
                        . "<td>" . $each['memo'] . "</td>";
                echo "</tr>";
            }
         ?>
    </table>

  </body>
</html>
