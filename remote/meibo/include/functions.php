
<?php
    function initDB(){
        $DB_DSN = "mysql:host=localhost; dbname=rhigashigawa; chartset=utf8";    // host=localhost・・・自分のですよ。utf8のところが違うと文字化けするよ。
        $DB_USER = "webaccess";
        $DB_PW = "toMeu4rH";      // IDとPWがめちゃめちゃ大事！！！見ることが出来る人・編集できる人を厳格に定義。
        $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
        return $pdo;
    }

    function getGrade(){
        $pdo = initDB();
        $query_str = "SELECT * FROM grade_master order by ID"; //ほんまはSELECTの後は要素を全部書いたほうがいいらしい
        $sql = $pdo -> prepare($query_str);   // $query_strに$pdoを入れる
        $sql -> execute();
        return $sql -> fetchAll();
    }

    function getSection(){
        $pdo = initDB();
        $query_str = "SELECT * FROM section1_master order by ID";
        $sql = $pdo -> prepare($query_str);
        $sql -> execute();
        return $sql -> fetchAll();
    }

?>
