function conf(){
    var temp_name = document.mainform.namae.value;  // "var"は変数であることを宣言する
    temp_name = temp_name.replace(/\s+/g,"");   //空白を取り除く処理サニタイズ。temp_nameにはストリング(文字列)が入る。temp_nameの空白を文字列0に置き換える。
    document.mainform.namae.value = temp_name;  // /\s+/gは「正規表現」という。空白を消した上でtemp_nameに置き換えるという処理に上書きする。
    if(temp_name == ""){
        alert("名前は必須です。");
        return false;
    }

    var temp_pref = document.mainform.pref.value;
    if(temp_pref == ""){
        alert("出身地を選択してください。");
        return false;
    }

    var temp_age = document.mainform.age.value;
    temp_age = temp_age.replace(/\s+/g,"");
    document.mainform.age.value = temp_age;
    if(temp_age == "" || Number.isInteger(temp_age) || temp_age < 1 || temp_age >= 100){ // ||はORを表す。&&はAND。
        alert("年齢は必須です。/ 数値を入力してください。/ 1～99の範囲で入力してください。");
        return false;
    }

    if(window.confirm('更新をします。よろしいですか？')){
        document.mainform.submit();
    }
    //alert("Hello, JavaScript.");
    /*var element = document.getElementById("yoisho");
    element.textContent = "ビール腹";*/
}
