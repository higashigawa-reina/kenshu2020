<!DOCTYPE html>
<link rel = "stylesheet" type="text/css" href = "./include/style.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<?php
    include("./include/statics.php");
    include("./include/functions.php");
    $pdo = initDB();

    $query_str = "SELECT m.member_ID, m.name, m.seibetu, m.age, sm.section_name, gm.grade_name
	              FROM member AS m
                  LEFT JOIN section1_master AS sm ON m.section_ID = sm.ID
                  LEFT JOIN grade_master AS gm ON m.grade_ID = gm.ID
                  WHERE 1=1";

    if(isset($_GET['namae']) && $_GET['namae'] != ""){
      $query_str .= " AND m.name LIKE '%" . $_GET['namae'] . "%' ";
    }
    if(isset($_GET['seibetsu']) && $_GET['seibetsu'] != ""){
      $query_str .= " AND m.seibetu =" . $_GET['seibetsu'] ;
    }
    if(isset($_GET['busho']) && $_GET['busho'] != ""){
      $query_str .= " AND m.section_ID =" . $_GET['busho'] ;
    }
    if(isset($_GET['yakushoku']) && $_GET['yakushoku'] != ""){
      $query_str .= " AND m.grade_ID =" . $_GET['yakushoku'] ;
    }

    //echo $query_str;  //デバックプリント
    $sql = $pdo -> prepare($query_str);   // $query_strに$pdoを入れる
    $sql -> execute();
    $result = $sql -> fetchAll();

    $temp_name = '';
    if(isset($_GET['namae'])){
        $temp_name = $_GET['namae'];
    }

    $temp_seibetsu = '';
    if(isset($_GET['seibetsu'])){
        $temp_seibetsu = $_GET['seibetsu'];
    }

    $temp_busho = '';
    if(isset($_GET['busho'])){
        $temp_busho = $_GET['busho'];
    }

    $temp_yakushoku = '';
    if(isset($_GET['yakushoku'])){
        $temp_yakushoku = $_GET['yakushoku'];
    }
  ?>


<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device=width, initial-scale-1">
    <title>社員名簿</title>
    <script type="text/javascript">
        <!--
        function clearForm(){
            document.searchForm.namae.value = "";
            document.searchForm.seibetsu.value = "";
            document.searchForm.busho.value = "";
            document.searchForm.yakushoku.value = "";
        }
            -->
    </script>
  </head>
  <body>
      <?php include("./include/header.php"); ?>
      <form method="get" action="index.php" name="searchForm" id=button>
        <div style="display:inline; padding-right: 10px;" class="form-inline">
        名前：
        <input type='text' name='namae' maxlength='30' placeholder='検索したい名前を入力'
        class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" style="width:20%"
        value='<?php echo $temp_name;?>'>
        </div>
        <div style="display:inline; padding-right: 10px;" class="form-inline">
        性別：
        <select name='seibetsu' class="form-control" id="exampleFormControlSelect1" style="width:10%">
            <option value='' <?php if($temp_seibetsu ==''){echo 'selected';}?>>すべて</option>
             <?php
                foreach ($gender_array as $key => $value) {
                    if($temp_seibetsu == $key){
                        echo "<option value='" . $key . "'selected>" . $value . "</option>";
                    }else {
                        echo "<option value='" . $key . "'>" . $value . "</option>";
                    }
                }
               ?>
        </select>
        </div>
        <div style="display:inline; padding-right: 10px;" class="form-inline">
        部署：
        <select name='busho' class="form-control" id="exampleFormControlSelect1" style="width:10%">
           <option value="" <?php if($temp_busho == ""){echo 'selected';}?>>すべて</option>
             <?php
               $result_s = getSection();
                foreach ($result_s as $each){
                    if($temp_busho == $each['ID']){
                        echo "<option value='" . $each['ID'] . "' selected>" . $each['section_name'] . "</option>";
                    }else {
                        echo "<option value='" . $each['ID'] . "'>" . $each['section_name'] . "</option>";
                    }
                }
              ?>
        </select>
        </div>
        <div style="display:inline" class="form-inline">
        役職：
        <select name='yakushoku' class="form-control" id="exampleFormControlSelect1" style="width:13%">
           <option value='' <?php if($temp_yakushoku == ''){echo 'selected';}?>>すべて</option>
             <?php
               $result_g = getGrade();
                foreach ($result_g as $each){
                    if($temp_yakushoku == $each['ID']){
                        echo "<option value='" . $each['ID'] . "' selected>" . $each['grade_name'] . "</option>";
                    }else {
                        echo "<option value='" . $each['ID'] . "'>" . $each['grade_name'] . "</option>";
                    }
                }
              ?>
        </select>
        </div>
        <br/>
        <div style="padding:10px">
        <button type="submit" class="btn btn-outline-info">検索</button>
        <button type="button" class="btn btn-outline-secondary" onclick="clearForm();">リセットする</button>
        </div>
      </form>
      <hr/>
      <?php
        echo "<div style='margin-left: 200px;'>検索結果：" . count($result) . "件</div>"
       ?>
      <table class="table" style="text-align:center; width: 75%;">
        <thead>
          <tr>
              <th scope="col">社員ID</th><th scope="col">名前</th><th scope="col">部署名</th><th scope="col">役職名</th>
          </tr>
        </thead>
        <tbody>
          <?php
              if(count($result) == 0){
                  echo "<tr><td colspan='4'>検索結果なし</td></tr>";
              }else{
                  foreach ($result as $each) {
                      echo "<tr>";
                          echo "<th scope='row' style='width: 75px;'>" . $each['member_ID'] . "</td>"
                              . "<td style='width:150px'><a href = './detail01.php?member_ID=" . $each['member_ID'] . "'>" . $each['name'] . "</a></td>"
                              . "<td style='width:100px'>" . $each['section_name'] . "</td>"
                              . "<td style='width:100px'>" . $each['grade_name'] . "</td>";
                      echo "</tr>";
                }
              }
           ?>
       </tbody>
      </table>
      <pre><?php //var_dump($result); ?></pre>
      <pre><?php //var_dump($_GET); ?></pre>


      <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
      </body>
  </html>
